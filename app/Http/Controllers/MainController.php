<?php

namespace App\Http\Controllers;

class MainController extends Controller
{
    // Get the SPA view.
    public function __invoke()
    {
        return view('main');
    }
}
