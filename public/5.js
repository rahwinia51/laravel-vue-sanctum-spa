(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Welcome2.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Welcome2.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _plugins_i18n__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../plugins/i18n */ "./resources/js/plugins/i18n.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {},
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])({
    locale: 'lang/locale',
    locales: 'lang/locales'
  })),
  data: function data() {
    return {
      title: 'title'
    };
  },
  methods: {
    login: function login() {
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('sanctum/csrf-cookie').then(function () {
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('api/login', {
          email: 'jo1@example.com',
          password: 'password'
        }).then(function (res) {
          console.log(res);
        });
      });
    },
    user: function user() {
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('api/user').then(function (res) {
        console.log(res);
      });
    },
    logout: function logout() {
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('api/logout').then(function (res) {
        console.log(res);
      });
    },
    cookie: function cookie() {
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('sanctum/csrf-cookie').then(function (res) {
        console.log('cookie response', res);
      });
    },
    post: function post() {
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('posts', {
        data: 'data!!'
      }).then(function (res) {
        console.log('post response', res);
      });
    },
    axios2: function axios2() {
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('axios2').then(function (res) {
        console.log('axios2 response', res);
      });
    },
    setLocale: function setLocale(locale) {
      if (this.$i18n.locale !== locale) {
        Object(_plugins_i18n__WEBPACK_IMPORTED_MODULE_2__["loadMessages"])(locale);
        this.$store.dispatch('lang/setLocale', {
          locale: locale
        });
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._v("\n    Welcome Page " + _vm._s(_vm.title) + "\n    "),
    _c("div", { staticClass: "m-2" }, [
      _vm._v("\n        " + _vm._s(_vm.locale) + "\n    ")
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "m-2" }, [
      _vm._v("\n        " + _vm._s(_vm.$t("error_alert_text")) + "\n    ")
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "m-2" },
      _vm._l(_vm.locales, function(locale, key) {
        return _c(
          "button",
          {
            key: key,
            staticClass: "flex items-center border p-2 border-teal-500",
            on: {
              click: function($event) {
                return _vm.setLocale(key)
              }
            }
          },
          [
            _c("fa", { attrs: { icon: ["fas", "language"], size: "2x" } }),
            _vm._v(" "),
            _c("span", { staticClass: "pl-1" }, [_vm._v(_vm._s(locale))])
          ],
          1
        )
      }),
      0
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "bg-gray-200" },
      [
        _c(
          "button",
          {
            staticClass: "border p-4 border-teal-500 ",
            on: { click: _vm.login }
          },
          [
            _c(
              "span",
              [
                _c("fa", { attrs: { icon: ["far", "user"] } }),
                _vm._v(" LOGIN")
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "border p-4 border-blue-500 ",
            on: { click: _vm.user }
          },
          [
            _c(
              "span",
              [_c("fa", { attrs: { icon: ["far", "user"] } }), _vm._v(" USER")],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "border p-4 border-blue-500 ",
            on: { click: _vm.logout }
          },
          [
            _c(
              "span",
              [
                _c("fa", { attrs: { icon: ["far", "user"] } }),
                _vm._v(" LOGOUT")
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "border p-4 border-blue-500 ",
            on: { click: _vm.cookie }
          },
          [
            _c(
              "span",
              [
                _c("fa", { attrs: { icon: ["far", "user"] } }),
                _vm._v(" COOKIE")
              ],
              1
            )
          ]
        ),
        _vm._v(" "),
        _c("router-link", { attrs: { to: { name: "page1" } } }, [
          _c("button", { staticClass: "border p-4 border-blue-500 " }, [
            _c(
              "span",
              [
                _c("fa", { attrs: { icon: ["far", "user"] } }),
                _vm._v(" LINK1")
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c(
          "button",
          {
            staticClass: "border p-4 border-blue-500 ",
            on: { click: _vm.axios2 }
          },
          [
            _c(
              "span",
              [
                _c("fa", { attrs: { icon: ["far", "user"] } }),
                _vm._v(" AXIOS2")
              ],
              1
            )
          ]
        )
      ],
      1
    ),
    _vm._v(" "),
    _c("div", [_c("router-view")], 1)
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/pages/Welcome2.vue":
/*!*****************************************!*\
  !*** ./resources/js/pages/Welcome2.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Welcome2_vue_vue_type_template_id_d97e58ce_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true& */ "./resources/js/pages/Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true&");
/* harmony import */ var _Welcome2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Welcome2.vue?vue&type=script&lang=js& */ "./resources/js/pages/Welcome2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Welcome2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Welcome2_vue_vue_type_template_id_d97e58ce_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Welcome2_vue_vue_type_template_id_d97e58ce_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "d97e58ce",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Welcome2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Welcome2.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/pages/Welcome2.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Welcome2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Welcome2.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Welcome2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Welcome2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true&":
/*!************************************************************************************!*\
  !*** ./resources/js/pages/Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Welcome2_vue_vue_type_template_id_d97e58ce_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Welcome2.vue?vue&type=template&id=d97e58ce&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Welcome2_vue_vue_type_template_id_d97e58ce_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Welcome2_vue_vue_type_template_id_d97e58ce_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);