(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ "./resources/js/lang/kr.json":
/*!***********************************!*\
  !*** ./resources/js/lang/kr.json ***!
  \***********************************/
/*! exports provided: ok, cancel, error_alert_title, error_alert_text, token_expired_alert_title, token_expired_alert_text, login, register, join_us, page_not_found, go_home, logout, email, remember_me, password, forgot_password, confirm_password, submit, name, toggle_navigation, home, you_are_logged_in, reset_password, send_password_reset_link, settings, profile, your_info, info_updated, update, your_password, password_updated, new_password, login_with, register_with, verify_email, send_verification_link, resend_verification_link, failed_to_verify_email, verify_email_address, default */
/***/ (function(module) {

module.exports = JSON.parse("{\"ok\":\"확인\",\"cancel\":\"취소\",\"error_alert_title\":\"이런..\",\"error_alert_text\":\"오류가 발생했습니다. 다시 시도해주세요.\",\"token_expired_alert_title\":\"세션이 종료되었습니다.\",\"token_expired_alert_text\":\"다시 로그인 해주세요.\",\"login\":\"로그인\",\"register\":\"회원가입\",\"join_us\":\"회원으로 가입하시겠습니까?\",\"page_not_found\":\"페이지를 찾을 수 없습니다.\",\"go_home\":\"홈으로\",\"logout\":\"로그아웃\",\"email\":\"이메일\",\"remember_me\":\"기억하기\",\"password\":\"비밀번호\",\"forgot_password\":\"비밀번호 찾기\",\"confirm_password\":\"비밀번호 확인\",\"submit\":\"확인\",\"name\":\"이름\",\"toggle_navigation\":\"토글 메뉴\",\"home\":\"홈\",\"you_are_logged_in\":\"로그인 되어 있습니다!\",\"reset_password\":\"비밀번호 초기화\",\"send_password_reset_link\":\"비밀번호 초기화 링크 보내기\",\"settings\":\"설정\",\"profile\":\"프로필\",\"your_info\":\"정보\",\"info_updated\":\"정보가 수정되었습니다!\",\"update\":\"수정\",\"your_password\":\"비밀번호\",\"password_updated\":\"비밀번호가 변경되었습니다!\",\"new_password\":\"새로운 비밀번호\",\"login_with\":\"로그인하기\",\"register_with\":\"회원가입\",\"verify_email\":\"이메일 인증\",\"send_verification_link\":\"인증 메일 보내기\",\"resend_verification_link\":\"인증 메일을 다시 보내시겠습니까?\",\"failed_to_verify_email\":\"이메일 인증에 실패하였습니다.\",\"verify_email_address\":\"인증 메일을 보냈습니다. 링크를 확인해주세요.\"}");

/***/ })

}]);