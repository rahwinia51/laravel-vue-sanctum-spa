import Vue from 'vue'
import store from './store'
import router from './router'
import i18n from './plugins/i18n'
import App from './components/App'

import './components'
import './plugins'

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default)
// Vue.component('basic', require('./layouts/basic').default)

Vue.config.productionTip = false

new Vue({
    store,
    router,
    i18n,
    ...App
})
