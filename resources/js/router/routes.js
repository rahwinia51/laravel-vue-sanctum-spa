function page (path) {
    return () => import(`../pages/${path}`).then(m => m.default || m)
}

export default [
    { path: '/', name: 'welcome', component: page('Welcome.vue') },
    // { path: '/link1', name: 'link', component: page('Link1') },
    // { path: '/link1',
    //     component: page('Link1'),
    //     children: [
    //         { path: '', redirect: { name: 'page1' } },
    //         { path: 'page1', name: 'page1', component: page('Page1') },
    //         { path: 'page2', name: 'page2', component: page('Page2') }
    //     ]
    // },
    { path: '/login', name: 'login', component: page('auth/Login.vue') },
    { path: '/register', name: 'register', component: page('auth/Register.vue') },
    // { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
    // { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
    // { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
    // { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

    { path: '/home', name: 'home', component: page('Home.vue') },
    { path: '/settings',
        component: page('settings/Index.vue'),
        children: [
            { path: '', redirect: { name: 'settings.profile' } },
            { path: 'profile', name: 'settings.profile', component: page('settings/Profile.vue') },
            { path: 'password', name: 'settings.password', component: page('settings/Password.vue') }
        ] },

    { path: '*', component: page('errors/404.vue') }
]


