// window.axios = require('axios')
import axios from 'axios'
// import Swal from "sweetalert2"
import store from "../store"
// import i18n from "./i18n"
// import router from '../router'

// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
// axios.defaults.withCredentials = true

// Request interceptor
axios.interceptors.request.use(request => {
    const locale = store.getters['lang/locale']
    if (locale) {
        request.headers.common['Accept-Language'] = locale
    }

    // request.headers['X-Socket-Id'] = Echo.socketId()

    return request
})

// // Response interceptor
// axios.interceptors.response.use(response => response, error => {
//     const { status } = error.response
//
//     if (status >= 500) {
//         Swal.fire({
//             type: 'error',
//             title: i18n.t('error_alert_title'),
//             text: i18n.t('error_alert_text'),
//             reverseButtons: true,
//             confirmButtonText: i18n.t('ok'),
//             cancelButtonText: i18n.t('cancel')
//         })
//     }
//     // && store.getters['auth/check']
//     if (status === 401) {
//         Swal.fire({
//             type: 'warning',
//             title: i18n.t('token_expired_alert_title'),
//             text: i18n.t('token_expired_alert_text'),
//             reverseButtons: true,
//             confirmButtonText: i18n.t('ok'),
//             cancelButtonText: i18n.t('cancel')
//         }).then(() => {
//             store.commit('auth/LOGOUT')
//
//             //router.push({ name: 'login' })
//         })
//     }
//
//     return Promise.reject(error)
// })
