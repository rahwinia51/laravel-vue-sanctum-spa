import Vue from 'vue'
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {} from '@fortawesome/free-regular-svg-icons'
import {faLock, faSignOutAlt, faCog, faLanguage, faAngleDown, faKey, faUser} from '@fortawesome/free-solid-svg-icons'
import {faGithub} from '@fortawesome/free-brands-svg-icons'

library.add(faUser, faLock, faSignOutAlt, faCog, faGithub, faLanguage, faAngleDown, faKey, faUser)

Vue.component('fa', FontAwesomeIcon)
