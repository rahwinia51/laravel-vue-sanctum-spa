@php
$config = [
    'appName' => config('app.name'),
    'locale' => $locale = app()->getLocale(),
    'locales' => config('app.locales'),
    //'githubAuth' => config('services.github.client_id'),
];
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }}</title>
    {{-- Load the application css --}}
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    {{-- Load the application scripts --}}
    <script src="{{ mix('js/app.js') }}" defer></script>

</head>
<body>
<div id="app"></div>
</body>
{{-- Global configuration object --}}
<script>
    window.config = @json($config);
</script>
</html>

